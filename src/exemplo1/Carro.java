/**
 * Exemplo1: Programacao com threads
 * Autor: Reinaldo Fernandes
 * Ultima modificacao: 07/08/2017
 */
package exemplo1;

import java.util.Random;

/**
 *
 * @author Reinaldo Fernandes
 */
public class Carro implements Runnable {
    private static int KEY=0; // Crie no construtor da classe um identificador único para cada thread.
    private int ID;
  
    private final String taskName; //nome da tarefa
    private String resumoVoltas = "";
    private int tempoTotal = 0;
    private final static Random generator = new Random();
    private final SharedArray v;
    public Carro(String name, SharedArray v){
        this.ID = KEY;
        KEY ++;
        taskName = name+" "+ID;
        this.v = v; 
    }
    
    public void run(){
        try{
            for (int i=1; i<=4; i++){
                int tempoVolta = generator.nextInt(2000);
                Thread.sleep(tempoVolta);
                this.tempoTotal += tempoVolta;
                this.resumoVoltas += String.format("% 5d", tempoVolta)+"ms \t|";    
            }
            
        } catch (InterruptedException ex){
            System.out.printf("%s %s\n", taskName, "terminou de maneira inesperada.");
        }
        
        v.insert(taskName, resumoVoltas, ID, tempoTotal);
    }
       
}
