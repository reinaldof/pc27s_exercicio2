/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exemplo1;

/**
 *
 * @author Reinaldo Fernandes
 */
public class SharedArray {
    public class Item{
        private int time;
        private String name;
        private String rel;
    }
    
    private Item[] elements;
    
    public SharedArray(int size){
        elements = new Item[size];
    }
    
    public synchronized void insert(String name, String rel, int index, int time){
        this.elements[index] = new Item();
        this.elements[index].time = time;
        this.elements[index].name = name;
        this.elements[index].rel = rel;
        
    } 
    
    public int getTime(int index){
        return elements[index].time;
    }
    
    public int getIndexShorterTime(){
        int r = 0;
        for (int i=0; i<elements.length; i++){
            if (getTime(i)<getTime(r)){
                r = i;
            }
        }
        return r;
    }
    
    public Item getShorterTime(){
        return elements[getIndexShorterTime()];
    }
    
    public void printShorterTimeInfo(){
        Item m = getShorterTime();
       System.out.println( "\nVencedor-> "+m.name+" com "+m.time+"ms\n"); 
    }
    
    public void printArrayInfo(){
         System.out.println("Carro\t| Volta 1\t| Volta 2\t| Volta 3\t| Volta 4\t| Tempo total");
        for (int i=0; i<elements.length; i++){
            System.out.println(elements[i].name+"\t| "+elements[i].rel+" "+elements[i].time+"ms");
        } 
    }
}
